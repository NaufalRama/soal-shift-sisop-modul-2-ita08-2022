#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <dirent.h>

void gaweDiretoryAir()
{
    char *argv1[] = {"mkdir", "air", NULL};
    execv("/bin/mkdir",argv1); 
}

void extractZip()
{
    char *extract[] = {"unzip", "-qo","animal.zip", NULL};
    execv("/bin/unzip", extract);  
}

void soal3c() {
  DIR *pDir;
  struct dirent *pDirent;
  pDir = opendir("animal");
  if (pDir != NULL) 
  {
    while(pDirent = readdir(pDir)) 
    {
      // SORTING FILE
      if (strcmp(pDirent->d_name, ".") != 0 && strcmp(pDirent->d_name, "..") != 0) {
        if (fork() == 0) { 
          char *hewanDarat = strstr(pDirent->d_name, "darat");
          char *hewanAir = strstr(pDirent->d_name, "air");
          // PINDAH FILE
        }
      }
    }
    (void) closedir(pDir);
  } 
  else 
  {
    exit(0);
  }
}


int main(int argc, char *argv[])
{
    pid_t child_id;
    int status;

    child_id = fork();

    if(child_id < 0)
    {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
    // this is child
        while (wait(&status) > 0);
        int i = 1;
        while (i < 8)
        {
            if (fork() == 0) 
            {
                if (i == 1) 
                {
                    sleep(3);
                    gaweDirectoryAir();
                }
                else if (i == 2) 
                {
                    extractZip();
                }
                if (i == 3)
                {
                    soal3c();
                } 
                
            }
            i++;
        }
        
        
    } 
    else {
    // this is parent
        //BUAT FOLDER DARAT
        char *argv1[] = {"mkdir", "darat", NULL};
        execv("/bin/mkdir",argv1);        
    }

    return 0;
}

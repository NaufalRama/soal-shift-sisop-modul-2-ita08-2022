# soal-shift-sisop-modul-2-ITA08-2022



Anggota Kelompok :
1. Ilham Muhammad Sakti - 5027201042
2. Gennaro Fajar Mennde - 5027201061
3. Naufal Ramadhan - 5027201067

## Daftar Isi
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)

## Soal 1
1.	Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu. 

### Soal 1a.
Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut.

### Pembahasan Soal 1a
Pada soal ini, pertama sekali kami melakukan deklarasi variable `char_link` dan `weapon_link` yang akan berfungsi sebagai nama variable dari link dowload file.
```c
char *char_link = "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp";
char *weapon_link = "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT";
```

Lalu, kami melakukan pengkondisian if dengan bantuan fungsi `exists` untuk mengecek ketersediaan file.
```c
int exists(const char *fname)
{
    DIR* dir = opendir(fname);
    if (dir){
        closedir(dir);
        return 1;
    } else if(ENOENT == errno){
        return 0;
    }
}
```

Apabila pada folder tidak ditemukan file bernama characters dan weapon maka akan mendownload file dari link yang ada lalu disimpan di dalam folder dengan nama "char.zip" dan "weapon.zip". Ketika menjalankan perintah tersebut, kami menggunakan fungsi "runCommand". Pada fungsi tersebut, akan melakukan spawning karena pada saat bersamaan tidak dapat melakukan 2 proses download dalam 1 waktu. Oleh sebab itu, kami menggunakan fork terlebih dahulu.
```c
void runCommand(char command[], char *lines[]){
    int status;
    pid_t child_id=fork();
    if(child_id==0)
        execv(command, lines);
    while(wait(&status)>0);
}
```
Setelah berhasil melakukan download, maka kami melakukan unzip file. Program yang dibuat juga mirip dengan cara mendownload. Hanya fungsinya saja yang berbeda. Pada langkah ini juga kami menggunakan bantuan fungsi "runCommand" karena file yang akan diekstrak terdapat sebanyak 2 file.
```c
int main(){
    // Download database
    if((exists("characters") == 0) && (exists("weapons") == 0)){
		char *getChar[] = {"wget", char_link, "-O", "char.zip", "-o", "/dev/null", NULL};
			runCommand("/usr/bin/wget", getChar);
        char *getWeapon[] = {"wget", weapon_link, "-O", "weapon.zip", "-o", "/dev/null", NULL};
			runCommand("/usr/bin/wget", getWeapon);
    }
   	// Extact File
   	char *extractFileChar[] = {"unzip", "char.zip", "-d", "char", NULL};
    runCommand("/bin/unzip", extractFileChar);
	char *extractFileWeapon[] = {"unzip", "weapon.zip", "-d", "weapon", NULL};
    runCommand("/bin/unzip", extractFileWeapon);
}
```

## Screenshot Hasil
![Gambar Hasil 1a](./img/DirHasil.jpeg)
![Gambar Hasil 1a](./img/DirCharacter.jpeg)
![Gambar Hasil 1a](./img/IsiDirChara.jpeg)
![Gambar Hasil 1a](./img/DirWeapon.jpeg)
![Gambar Hasil 1a](./img/IsiDirWeapon.jpeg)
## Kendala
- Kami mengalami error ketika tidak menggunakan fork. 
- Ketika program dijalankan file yang terdownload hanya 1. 
- Tidak dapat mendownload file secara bersamaan pada 1 proses.

## Soal 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

## Pembahasan Soal 2
Secara garis besar soal 2 ini kita diminta untuk melakukan proses unzip sebuah file zip drakor dimana di dalamnya terdapat beberapa file `.png` dan kita diminta untuk melakukan kategorisasi berdasarkan genre dari masing-masing drakor. Pertama kita dapat membuat sebuah string global dimana string tersebut dapat digunakan untuk melakukan perintah `execv`. Untuk stringnya seperti dibawah.
```sh
char keyMkdir[] = "/bin/mkdir";
char keyUnzip[] = "/usr/bin/unzip";
char keyRemove[] = "/usr/bin/rm";
char keyCopy[] = "/usr/bin/cp";
char keyMove[] = "/usr/bin/mv";
char keyTouch[] = "/usr/bin/touch";
```

Kemudian kita dapat melakukan perintah `execv` dan dapat dilakukan berulang kali dalam `main()`. Untuk perintahnya seperti dibawah.
```sh
void createProcess(char *str, char *argv[]){
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id == 0)
  {
    execv(str, argv);
  }
  else
  {
    while (wait(&status) > 0)
      ;
  }
}
```

Untuk seluruh fungsi di dalam `main()` seperti dibawah ini.
```sh
int main(){
  createParentFolder();
  extractZip();
  removeDir();
  createFolderDrakorByCategory();
  moveFileByCategory();
  getListFolderDrakor();
}
```

### Soal 2a.
Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

### Pembahasan soal 2a.
Hal pertama yang dilakukan adalah membuat folder drakor di dalam home dimana nantinya file zip akan otomatis di ekstrak di dalamnya oleh program. Untuk fungsi membuat directorynya seperti dibawah.
```sh
void createParentFolder(){
  char *argv[] = {"mkdir", "-p", "/home/naufalrama/shift2/drakor/", NULL};
  createProcess(keyMkdir, argv);
}
```
Setelah folder terbentuk, kita dapat mengekstrak file zip ke dalam folder/directory yang sudah terbentuk tadi. Untuk pathnya dapat diarahkan ke alamat directory sebelumnya seperti dibawah ini.
```sh
void extractZip(){
  char *argv[] = {"unzip", "-qo", "/home/naufalrama/Documents/SISOP2022/Modul2/Soal2/drakor.zip", "-d", "/home/naufalrama/shift2/drakor", NULL};
  createProcess(keyUnzip, argv);
```
Untuk path yang pertama adalah dimana kami meletakkan file zip tersebut dan akan diarahkan ke path dari directory drakor yang tadi kita buat.

Kemudian setelah kita dapat mengekstrak file zip tersebut, kita diminta untuk menghapus folder-folder yang tidak penting. Untuk fungsi yang kami gunakan seperti dibawah ini.
```sh
void removeDir(){
  char *argv[] = {"rm", "-r", "/home/naufalrama/shift2/drakor/coding/", "/home/naufalrama/shift2/drakor/song/", "/home/naufalrama/shift2/drakor/trash", NULL};
  createProcess(keyRemove, argv);
}
```

### Soal 2b.
Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam “/drakor/romance”, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.

### Pembahasan soal 2b.
Pada soal 2b ini diminta untuk melakukan kategorisasi berdasarkan genre dari masing-masing drakor. Untuk fungsinya kita akan menggunakan `createFolderDrakorByCategory()`. Maka akan tercipta beberapa folder sesuai dengan genre yang dibutuhkan. Untuk programnya seperti dibawah.
```sh
void createFolderDrakorByCategory()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/naufalrama/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char corePath[100] = "/home/naufalrama/shift2/drakor/";
          strcat(corePath, token3);
          char *createFolder3[] = {"mkdir", "-p", corePath, NULL};
          createProcess("/bin/mkdir", createFolder3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char corePath1[100] = "/home/naufalrama/shift2/drakor/";
          char corePath2[100] = "/home/naufalrama/shift2/drakor/";
          strcat(corePath1, token53);
          strcat(corePath2, token55);
          char *createFolder53[] = {"mkdir", "-p", corePath1, NULL};
          char *createFolder55[] = {"mkdir", "-p", corePath2, NULL};
          createProcess(keyMkdir, createFolder53);
          createProcess(keyMkdir, createFolder55);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

### Soal 2c.
Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.

### Pembahasan 2c.
Pada soal 2c ini setelah tercipta folder dari masing-masing genre, kita diminta untuk menyortir masing-masing file `.png` sesuai dengan genrenya. FUngsi yang digunakan pada program kali ini adalah `moveFileByCategory()`. Untuk programnya seperti dibawah ini.
```sh
void moveFileByCategory()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/naufalrama/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        strcpy(fileName, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char corePath[100] = "/home/naufalrama/shift2/drakor/";
          char destination[100];
          char fileSources[100];

          // copy the corePath
          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          // define destination
          strcat(destination, token3);
          strcat(destination, "/");

          // define file sources
          strcat(fileSources, fileName);

          char *moveFile3[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char corePath[100] = "/home/naufalrama/shift2/drakor/";
          char destination1[100];
          char destination2[100];
          char fileSources1[100];
          char fileSources2[100];

          // copy the corePath
          strcpy(destination1, corePath);
          strcpy(destination2, corePath);
          strcpy(fileSources1, corePath);
          strcpy(fileSources2, corePath);

          // define file sources
          strcat(fileSources1, fileName);
          strcat(fileSources2, fileName);

          // define destination1
          strcat(destination1, token53);
          strcat(destination1, "/");

          // define destination2
          strcat(destination2, token55);
          strcat(destination2, "/");

          char *copyFile53[] = {"cp", fileSources1, destination1, NULL};
          createProcess(keyCopy, copyFile53);
          char *moveFile55[] = {"mv", fileSources2, destination2, NULL};
          createProcess(keyMove, moveFile55);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

### Soal 2d dan 2e.
Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”.

Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending).

### Pembahasan 2d dan 2e.
Setelah kita menyortir seluruh file sesuai dengan genre dari masing-masing drakor. Maka kita diminta untuk melakukan sortir kembali dan me-rename seluruh file sesuai dengan ketentuan. Kemudian kita diminta untuk memindahkan ke dalam `data.txt` dimana isinya terdapat nama dan tahun rilis dari masing-masing drakor. Untuk program seperti dibawah ini.
```sh
void renameFileAndCreateTxtPerFolder(char *whereFolder)
{
  DIR *dp;
  struct dirent *ep;
  char path[100] = "/home/naufalrama/shift2/drakor/";
  strcat(path, whereFolder);

  dp = opendir(path);

  if (dp != NULL)
  {
    int arrTahun[5][1] = {};
    char arrFileName[5][100] = {};
    int idx = 0;

    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        char backupFilename[100];
        strcpy(fileName, ep->d_name);
        strcpy(backupFilename, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char corePath[100] = "/home/naufalrama/shift2/drakor/";
          strcat(corePath, whereFolder);
          strcat(corePath, "/");

          char destination[100];
          char fileSources[100];

          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          strcat(fileSources, fileName);

          strcat(destination, arr[0]);
          strcat(destination, ".png");

          arrTahun[idx][0] = atoi(arr[1]);
          strcpy(arrFileName[idx], arr[0]);
          idx++;

          char *moveFile3[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char corePath[100] = "/home/naufalrama/shift2/drakor/";
          strcat(corePath, whereFolder);
          strcat(corePath, "/");

          char destination[100];
          char fileSources[100];

          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          strcat(fileSources, fileName);

          char *file53 = strtok(arr[2], "_");
          char *file55 = strtok(arr[4], ".");

          if (strcmp(file53, whereFolder) == 0)
          {
            strcat(destination, arr[0]);
            strcat(destination, ".png");

            arrTahun[idx][0] = atoi(arr[1]);
            strcpy(arrFileName[idx], arr[0]);
            idx++;
          }
          else if (strcmp(file55, whereFolder) == 0)
          {
            char *token52 = strtok(backupFilename, "_");
            char *token522 = strtok(token52, ";");

            strcat(destination, token522);
            strcat(destination, ".png");

            arrTahun[idx][0] = atoi(arr[3]);
            strcpy(arrFileName[idx], token522);
            idx++;
          }

          char *moveFile[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile);
        }
      }
    }

    strcat(path, "/data.txt");
    char *createTxt[] = {"touch", path, NULL};
    createProcess(keyTouch, createTxt);

    FILE *f = fopen(path, "w");
    if (f == NULL)
    {
      printf("Error when opening file %s \n", path);
      exit(1);
    }

    fprintf(f, "kategori: %s \n\n", whereFolder);

    insertionSort(arrTahun, arrFileName, 5);

    for (int i = 0; i < 5; i++)
    {
      if (arrTahun[i][0] != 0)
      {
        fprintf(f, "nama: %s \n", arrFileName[i]);
        fprintf(f, "rilis: tahun %d \n\n", arrTahun[i][0]);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

Kemudian akan dilakukan list drakor dari masing-masing folder. Untuk program seperti dibawah.
```sh
void getListFolderDrakor()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/naufalrama/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        renameFileAndCreateTxtPerFolder(ep->d_name);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}
```

## Screenshot Hasil
![Gambar Hasil 2.a](./img/DirShift2.png)
![Gambar Hasil 2.a drakor](./img/DirDrakor.png)
![Gambar Hasil 2.b](./img/ListDirCategory.png)
![Gambar Hasil IsiDirectory](./img/ContohisiDir.png)
![Gambar Hasil data.txt](./img/Data.txt.png)

## Kendala
- Terkadang terdapat error pada saat membagi drakor pada masing-masing kategori
- Pada saat sorting nama dan tahun juga terkadang muncul error dimana nama tidak tersortir.

## Soal 3
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang.

### Soal 3a.
Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.

### Pembahasan 3a.
Membuat folder darat dan 3 detik berikutnya membuat folder air
Alamat directory “/home/[USER]/modul2/darat” dan “/home/[USER]/modul2/air”
```c
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pwd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
```

- `<stdio.h>` library untuk fungsi input-output (e.g. `printf()`, `sprintf()`)
- `<string.h>` library untuk melakukan manipulasi arrays of character (e.g. *memset())
- `<unistd.h>` library untuk melakukan system call kepada kernel linux (e.g. `fork()`)
- `<pwd.h>` library untuk mengakses database pengguna sistem
- `<dirent.h>` library untuk melakukan pemindahan directory.
- `<sys/stat.h>` library untuk melakukan pengembalian dari status waktu (e.g. `time_t()`)
- `<sys/types.h>` library tipe data khusus (e.g. pid_t)
- `<sys/wait.h>` library untuk melakukan wait (e.g. `wait()`)

```c
#define EXIT_FAILURE -1
#define EXIT_SUCCESS 0
```
Pendefinisian “EXIT_FAILURE -1” jika gagal membuat proses baru, program akan diberhentikan dan “EXIT_SUCCESS 0” jika proses baru berhasil dieksekusi.
```c
char url[] = "https://drive.google.com/uc?id=1LauKPhyLsUTNKJXo46Dj127OIMFwOVYY&export=download";
char fileName[] = "/home/ilham/modul2/animal.zip";
char dir[] = "/home/ilham/modul2";
char animalFolder[] = "/home/ilham/modul2/animal";
char *folder[] = {"/home/ilham/modul2/darat", "/home/ilham/modul2/air"};
char *fname[] = {"*darat*", "*air*"};
```
Pendefinisian alamat directory yang akan digunakan pada program.

```c
extern void exit();
```
Fungsi ini digunakan agar dapat menggunakan EXIT_FAILURE.
```c
int main()
{
	for(int i=0; i<2; i++)
	{
		createFolder(i);
	}
}
```
Memanggil fungsi createFolder() untuk membuat folder darat dan air.
```c
void createFolder(int index)
{
   	pid_t child_id = fork();
   	int status;
	if (child_id < 0)
       {
       	exit(EXIT_FAILURE);
   	}
}
```
Fungsi fork() berguna untuk membuat proses baru dan status digunakan untuk mendeteksi proses pada child sudah selesai atau belum.

Apabila proses baru gagal dibuat atau child_id < 0, maka program akan berhenti.
```c
if (child_id == 0)
    {
        char *argv[] = {"mkdir", "-p", folder[index], NULL};
    	execv("/bin/mkdir", argv);
     }
```
Apabila child_id = 0 , maka akan membuat sebuah folder sesuai inputan indexnya menggunakan fungsi execv. Jika indexnya nol maka akan membuat folder darat dan apabila indexnya satu maka akan membuat folder air.

```c
while(wait(&status) > 0);
```
Menunggu proses child selesai menjalankan tugasnya, kemudian parent process menjalankan proses berikutnya.

```c
 sleep(3);
```
Pemberian jeda 3 detik untuk proses berikutnya.

### Soal 3b.
Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

### Pembahasan 3b.
Mengekstrak “animal.zip”
```c
int main()
{
	…
	…
	…
download();
	extract();
/* Memanggil fungsi download() untuk mendownload file animal.zip dan extract() untuk mengekstrak file animal.zip. */
	…
	…
	…
}

void download()
{
	pid_t child_id;
        int status;

	child_id = fork();

/* Fungsi fork() berguna untuk membuat proses baru dan status digunakan untuk mendeteksi proses pada child sudah selesai atau belum. */

    if(child_id < 0)
    {
        exit(EXIT_FAILURE);
    }

/* Apabila proses baru gagal dibuat atau child_id < 0, maka program akan berhenti. */

	if(child_id == 0)
	{
    	char *argv[] = {"wget", "-q", url, "-O", fileName, NULL};
    	execv("/usr/bin/wget", argv);
	}

/* Apabila child_id = 0 , maka akan mendownload animal.zip dari gdrive s22-praktikan.  */

   	while(wait(&status) > 0);

/* Menunggu proses child selesai menjalankan tugasnya, kemudian parent process menjalankan proses berikutnya. */

}

void extract()
{
	pid_t child_id;
	int status;

	child_id = fork();

/* Fungsi fork() berguna untuk membuat proses baru dan status digunakan untuk mendeteksi proses pada child sudah selesai atau belum. */

	if(child_id < 0)
	{
		exit(EXIT_FAILURE);
	}

/* Apabila proses baru gagal dibuat atau child_id < 0, maka program akan berhenti. */

	if(child_id==0)
	{
    		char *argv[] = {"unzip", "-q", fileName, "-d", dir, NULL};
    		execv("/usr/bin/unzip", argv);
	}

/* Apabila child_id = 0 , maka akan mengekstrak file animal.zip  */

	while(wait(&status) > 0);

/* Menunggu proses child selesai menjalankan tugasnya, kemudian parent process menjalankan proses berikutnya. */

}
```

### Soal 3c.
Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

### Pembahasan 3c.
Pemisahan hewan darat dan air sesuai dengan nama filenya dan penghapusan file hewan yang tidak terdapat keterangan darat dan air.
```c
int main()
{
	…
	…
	…
for(int i=0; i<2; i++)
	{
		move(i);
	}
	removeFD(animalFolder, "f", "*.*");

/* Memanggil fungsi move()  untuk memindahkan hewan darat dan air ke direktorinya masing - masing dan kemudian memanggil fungsi removeFD() untuk menghapus hewan yang tidak memiliki keterangan darat maupun air.  */
	…
	…
	…
}

/* Fungsi move() berfungsi untuk memindahkan hewan darat dan air ke direktorinya masing-masing. */

void move(int idx)
{
	pid_t child_id;
	int status;

	child_id = fork();

/* Fungsi fork() berguna untuk membuat proses baru dan status digunakan untuk mendeteksi proses pada child sudah selesai atau belum. */

	if(child_id < 0)
    {
        exit(EXIT_FAILURE);
    }

/* Apabila proses baru gagal dibuat atau child_id < 0, maka program akan berhenti. */

	if(child_id == 0)
	{
		char *argv[] = {"find", animalFolder, "-type", "f", "-iname", fname[idx], "-exec", "mv", "{}", folder[idx], ";", NULL};
		execv("/usr/bin/find", argv);
	}

/* Apabila child_id = 0 , maka akan memindahkan hewan darat dan air ke direktorinya masing-masing.   */


	sleep(3);
	
/* Pemberian jeda 3 detik untuk proses berikutnya*/

	while(wait(&status) > 0 );

/* Menunggu proses child selesai menjalankan tugasnya, kemudian parent process menjalankan proses berikutnya. */

}

/* Fungsi remove() berfungsi untuk menghapus hewan tidak memiliki keterangan darat dan air.  Selain itu, fungsi ini menerima parameter alamatDirektori, tipe file (disini menggunakan “-f” yang artinya file reguler, dan keyword (artinya seluruh isi file).  */
void removeFD(char dirr[], char type[], char keyword[])
{
	pid_t child_id;
	int status;

	child_id = fork();

/* Fungsi fork() berguna untuk membuat proses baru dan status digunakan untuk mendeteksi proses pada child sudah selesai atau belum. */

    if(child_id < 0)
    {
        exit(EXIT_FAILURE);
    }

/* Apabila proses baru gagal dibuat atau child_id < 0, maka program akan berhenti. */

	if(child_id == 0)
	{
		char *argv[] = {"find", dirr, "-type", type, "-name", keyword, "-exec", "rm", "-r", "{}", "+", NULL};
		execv("/usr/bin/find", argv);
	}
	
/* Apabila child_id = 0 , maka akan hewan yang tidak memiliki keterangan darat dan air seluruh hewan yang tersisa di direktori animal.  */

	while(wait(&status) > 0);

/* Menunggu proses child selesai menjalankan tugasnya, kemudian parent process menjalankan proses berikutnya. */

}
```

### Soal 3d.
Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

### Pembahasan 3d.
Menghapus file hewan burung pada direktori darat.
```c
 int main()
{
	…
	…
	…
	removeFD(folder[0], "f", "*bird*");
/* Memanggil fungsi removeFD() untuk menghapus hewan burung pada direktori darat. */
	…
	…
	…
}

/* Fungsi remove() berfungsi untuk menghapus hewan burung pada direktori darat.  Selain itu, fungsi ini menerima parameter alamatDirektori, tipe file (disini menggunakan “-f” yang artinya file reguler, dan keyword (artinya seluruh file yang bernama bird).  */
void removeFD(char dirr[], char type[], char keyword[])
{
	pid_t child_id;
	int status;

	child_id = fork();

/* Fungsi fork() berguna untuk membuat proses baru dan status digunakan untuk mendeteksi proses pada child sudah selesai atau belum. */

    if(child_id < 0)
    {
        exit(EXIT_FAILURE);
    }

/* Apabila proses baru gagal dibuat atau child_id < 0, maka program akan berhenti. */

	if(child_id == 0)
	{
		char *argv[] = {"find", dirr, "-type", type, "-name", keyword, "-exec", "rm", "-r", "{}", "+", NULL};
		execv("/usr/bin/find", argv);
	}
	
/* Apabila child_id = 0 , maka akan hewan yang tidak memiliki keterangan darat dan air seluruh hewan yang tersisa di direktori animal.  */

	while(wait(&status) > 0);

/* Menunggu proses child selesai menjalankan tugasnya, kemudian parent process menjalankan proses berikutnya. */

}
```

### Soal 3e.
Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

### Pembahasan 3e.
Membuat file list.txt pada folder air dan membuat list semua hewan dengan format UID_[FILE PERMISSION]_NAMA FILE.jpg.
```c
int main()
{
	…
	…
	…
	createFile("/home/ilham/modul2/air/list.txt");
	listFiles(folder[1]);
/* Fungsi createFile() berfungsi untuk membuat file list.txt yang nantinya berisi nama semua hewan yang ada pada direktori air. Serta fungsi listFiles() berfungsi untuk menambahkan nama UID_[UID file permission]_NAMA FILE.[jpg/png] */
	…
	…
	…
}

/* Fungsi yang akan mengembalikan UID dari file masing-masing. Fungsi ini menerima parameter path direktori dari file yang dimaksud */
const char* getUID(char path[])
{
	struct stat info;
	int r;

	r = stat(path, &info);
    	if( r == -1 )
    	{
        	fprintf(stderr,"File error\n");
        	exit(EXIT_FAILURE);
/* Jika r == -1 maka akan mencetak string “File error” dan setelah itu program diberhentikan. */
    	}

    	struct passwd *pw = getpwuid(info.st_uid);

   	if (pw != 0)
	{
		return (pw->pw_name);
/* Mengembalikan UID file */
	}
	else
	{
		return "ERROR";
/* Mengembalikan ERROR, jika UID bukan sama dengan 0*/
	}
}










/* Fungsi permission() ini berfungsi untuk mencari permission dari file yang dimaksud. Disini formatnya adalah rwxrwxrwx. 
r akses untuk read atau membaca file
w akses untuk write atau memodifikasi file
x akses untuk execute atau melihat file
Dimana 3 huruf pertama menunjukkan permission untuk user, 3 huruf selanjutnya permission untuk group dan 3 huruf terakhir adalah other permission apabila tidak ada permission untuk r, w, ataupun x akan digantikan dengan “-”.
*/

const char* permission(char path[])
{
	struct stat fs;
	char filePermission[100] = {};
	char *sPermission = filePermission;
	int r;

	r = stat(path, &fs);
	if(r == -1)
	{
		fprintf(stderr, "File error\n");
		exit(EXIT_FAILURE);
/* Jika r == -1 maka akan mencetak string “File error” dan setelah itu program diberhentikan. */
	}

	//OWNER PERMISSIONS, penentuan permission pada owner pada file
	if(fs.st_mode & S_IRUSR)
		strcat(filePermission, "r");
	else
		strcat(filePermission, "-");

	if(fs.st_mode & S_IWUSR)
                strcat(filePermission, "w");
        else
                strcat(filePermission, "-");

	if( fs.st_mode & S_IXUSR )
        	strcat(filePermission, "x");
        else
                strcat(filePermission, "-");


	//GROUP PERMISSIONS, penentuan permission group pada file
	if( fs.st_mode & S_IRGRP )
        	strcat(filePermission, "r");
        else
                strcat(filePermission, "-");

    	if( fs.st_mode & S_IWGRP )
        	strcat(filePermission, "w");
        else
                strcat(filePermission, "-");

    	if( fs.st_mode & S_IXGRP )
        	strcat(filePermission, "x");
        else
                strcat(filePermission, "-");


	//OTHER PERMISSIONS, penentuan other permission pada file
	if( fs.st_mode & S_IROTH )
		strcat(filePermission, "r");
        else
                strcat(filePermission, "-");

	if( fs.st_mode & S_IWOTH )
		strcat(filePermission, "w");
        else
                strcat(filePermission, "-");

	if( fs.st_mode & S_IXOTH )
		strcat(filePermission, "x");
        else
                strcat(filePermission, "-");

	return sPermission;
}

/* Fungsi createFile() berfungsi untuk membuat file kosong (file list.txt)  */
void createFile(char fileName[])
{
	pid_t child_id;
	int status;

	child_id  = fork();

/* Fungsi fork() berguna untuk membuat proses baru dan status digunakan untuk mendeteksi proses pada child sudah selesai atau belum. */

    if(child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
/* Apabila proses baru gagal dibuat atau child_id < 0, maka program akan berhenti. */

    if(child_id == 0)
    {
        char *argv[] = {"touch", fileName, NULL};
		execv("/usr/bin/touch", argv);
    }

/* Apabila child_id = 0 , maka membuat file kosong.  */    

    while(wait(&status) > 0 );

/* Menunggu proses child selesai menjalankan tugasnya, kemudian parent process menjalankan proses berikutnya. */

}

/* Fungsi ini untuk menambahkan text pada file touch yang telah dibuat sebelumnya */
void printText(char text[])
{
	char *filename = "/home/ilham/modul2/air/list.txt";

	FILE *fp = fopen(filename, "a");
/* Buka file yang telah dibuat sebelumnya*/
	if(fp == NULL)
	{
		printf("ERROR SAAT MEMBUKA FILE\n");
		exit(EXIT_FAILURE);
	}
/* Jika NULL, maka akan menampilakn “ERROR SAAT MEMBUKA FILE” dan memberhentikan programnya. */

	fprintf(fp, "%s\n", text);
/* Memasukkan teks pada file kosong yang terlah dibuat sebelumnya  */

	fclose(fp);

/* Menutup file */

}

/* Fungsi listFiles()  berguna untuk list isi direktori air Fungsi ini akan memanggil fungsi lain yang dibutuhkan seperti UID dan permission File. */
void listFiles(char *basePath)
{
	char path[1000];
	struct dirent *dp;
	DIR *dir = opendir(basePath);

/* Membuka direktori air */

	if(!dir)
		return;
/* Jika dir sama dengan NULL  maka akan langsung return*/

	while((dp = readdir(dir)) != NULL)
	{
		if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
		{

			char fileUID[250];
			char filePermission[20];
			char tmpDir[250];
			char format[250];

			strcpy(tmpDir, folder[1]);
			strcat(tmpDir, "/");
			strcat(tmpDir, dp -> d_name);

			/* untuk mendapatkan UID pada file */

			strcpy(fileUID, getUID(tmpDir));

			/* untuk mendapatkan file permission */

			strcpy(filePermission, permission(tmpDir));

			/* menggabungkan UID, file permission dan nama file */

			strcpy(format, fileUID);
			strcat(format, "_");
			strcat(format, filePermission);
			strcat(format, "_");
			strcat(format, dp -> d_name);

			/* memasukkan format ke text file (list.txt) */
			if(strcmp(dp -> d_name, "list.txt") != 0)
				printText(format);

			/* membuat path baru dari base path */
			strcpy(path, basePath);
			strcat(path, "/");
			strcat(path, dp -> d_name);

			listFiles(path);

/* Menggabungkan nama user_UID_nama file.[jpg/ png] dan memasukkan nya ke file list.txt */

		}
	}

	closedir(dir);

/* Untuk menutup direktori air*/

}
```

## Screenshot Hasil
![Gambar Hasil No 3](./img/Picture1.png)
![Gambar Hasil No 3](./img/Picture2.png)
![Gambar Hasil No 3](./img/Picture3.png)
![Gambar Hasil No 3](./img/Picture4.png)

## Kendala
- Kesulitan dalam memodifikasi string untuk menentukan alamat nama file

[Kembali ke Daftar Isi](#daftar-isi)


#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>

char keyMkdir[] = "/bin/mkdir";
char keyUnzip[] = "/usr/bin/unzip";
char keyRemove[] = "/usr/bin/rm";
char keyCopy[] = "/usr/bin/cp";
char keyMove[] = "/usr/bin/mv";
char keyTouch[] = "/usr/bin/touch";

void createProcess(char *str, char *argv[])
{
  pid_t child_id;
  child_id = fork();
  int status;

  if (child_id == 0)
  {
    execv(str, argv);
  }
  else
  {
    while (wait(&status) > 0)
      ;
  }
}

void createParentFolder()
{
  char *argv[] = {"mkdir", "-p", "/home/naufalrama/shift2/drakor/", NULL};
  createProcess(keyMkdir, argv);
}

void extractZip()
{
  char *argv[] = {"unzip", "-qo", "/home/naufalrama/Documents/SISOP2022/Modul2/Soal2/drakor.zip", "-d", "/home/naufalrama/shift2/drakor", NULL};
  createProcess(keyUnzip, argv);
}

void removeDir()
{
  char *argv[] = {"rm", "-r", "/home/naufalrama/shift2/drakor/coding/", "/home/naufalrama/shift2/drakor/song/", "/home/naufalrama/shift2/drakor/trash", NULL};
  createProcess(keyRemove, argv);
}

void createFolderDrakorByCategory()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/naufalrama/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char corePath[100] = "/home/naufalrama/shift2/drakor/";
          strcat(corePath, token3);
          char *createFolder3[] = {"mkdir", "-p", corePath, NULL};
          createProcess("/bin/mkdir", createFolder3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char corePath1[100] = "/home/naufalrama/shift2/drakor/";
          char corePath2[100] = "/home/naufalrama/shift2/drakor/";
          strcat(corePath1, token53);
          strcat(corePath2, token55);
          char *createFolder53[] = {"mkdir", "-p", corePath1, NULL};
          char *createFolder55[] = {"mkdir", "-p", corePath2, NULL};
          createProcess(keyMkdir, createFolder53);
          createProcess(keyMkdir, createFolder55);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}

void moveFileByCategory()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/naufalrama/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        strcpy(fileName, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char *token3 = strtok(arr[2], ".");
          char corePath[100] = "/home/naufalrama/shift2/drakor/";
          char destination[100];
          char fileSources[100];

          // copy the corePath
          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          // define destination
          strcat(destination, token3);
          strcat(destination, "/");

          // define file sources
          strcat(fileSources, fileName);

          char *moveFile3[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char *token53 = strtok(arr[2], "_");
          char *token55 = strtok(arr[4], ".");
          char corePath[100] = "/home/naufalrama/shift2/drakor/";
          char destination1[100];
          char destination2[100];
          char fileSources1[100];
          char fileSources2[100];

          // copy the corePath
          strcpy(destination1, corePath);
          strcpy(destination2, corePath);
          strcpy(fileSources1, corePath);
          strcpy(fileSources2, corePath);

          // define file sources
          strcat(fileSources1, fileName);
          strcat(fileSources2, fileName);

          // define destination1
          strcat(destination1, token53);
          strcat(destination1, "/");

          // define destination2
          strcat(destination2, token55);
          strcat(destination2, "/");

          char *copyFile53[] = {"cp", fileSources1, destination1, NULL};
          createProcess(keyCopy, copyFile53);
          char *moveFile55[] = {"mv", fileSources2, destination2, NULL};
          createProcess(keyMove, moveFile55);
        }
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}

void insertionSort(int arrTahun[5][1], char arrFilename[5][100], int n)
{
  int i, key, j;
  char kuy[100];
  for (i = 1; i < n; i++)
  {
    key = arrTahun[i][0];
    strcpy(kuy, arrFilename[i]);
    j = i - 1;

    while (j >= 0 && arrTahun[j][0] > key)
    {
      arrTahun[j + 1][0] = arrTahun[j][0];
      strcpy(arrFilename[j + 1], arrFilename[j]);
      j = j - 1;
    }
    arrTahun[j + 1][0] = key;
    strcpy(arrFilename[j + 1], kuy);
  }
}

void renameFileAndCreateTxtPerFolder(char *whereFolder)
{
  DIR *dp;
  struct dirent *ep;
  char path[100] = "/home/naufalrama/shift2/drakor/";
  strcat(path, whereFolder);

  dp = opendir(path);

  if (dp != NULL)
  {
    int arrTahun[5][1] = {};
    char arrFileName[5][100] = {};
    int idx = 0;

    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        int i = 0;
        char arr[5][50] = {};
        char fileName[100];
        char backupFilename[100];
        strcpy(fileName, ep->d_name);
        strcpy(backupFilename, ep->d_name);
        char *token = strtok(ep->d_name, ";");

        while (token != NULL)
        {
          strcpy(arr[i], token);
          i++;
          token = strtok(NULL, ";");
        }

        if (i == 3)
        {
          char corePath[100] = "/home/naufalrama/shift2/drakor/";
          strcat(corePath, whereFolder);
          strcat(corePath, "/");

          char destination[100];
          char fileSources[100];

          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          strcat(fileSources, fileName);

          strcat(destination, arr[0]);
          strcat(destination, ".png");

          arrTahun[idx][0] = atoi(arr[1]);
          strcpy(arrFileName[idx], arr[0]);
          idx++;

          char *moveFile3[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile3);
        }
        else if (i == 5)
        {
          char corePath[100] = "/home/naufalrama/shift2/drakor/";
          strcat(corePath, whereFolder);
          strcat(corePath, "/");

          char destination[100];
          char fileSources[100];

          strcpy(destination, corePath);
          strcpy(fileSources, corePath);

          strcat(fileSources, fileName);

          char *file53 = strtok(arr[2], "_");
          char *file55 = strtok(arr[4], ".");

          if (strcmp(file53, whereFolder) == 0)
          {
            strcat(destination, arr[0]);
            strcat(destination, ".png");

            arrTahun[idx][0] = atoi(arr[1]);
            strcpy(arrFileName[idx], arr[0]);
            idx++;
          }
          else if (strcmp(file55, whereFolder) == 0)
          {
            char *token52 = strtok(backupFilename, "_");
            char *token522 = strtok(token52, ";");

            strcat(destination, token522);
            strcat(destination, ".png");

            arrTahun[idx][0] = atoi(arr[3]);
            strcpy(arrFileName[idx], token522);
            idx++;
          }

          char *moveFile[] = {"mv", fileSources, destination, NULL};
          createProcess(keyMove, moveFile);
        }
      }
    }

    strcat(path, "/data.txt");
    char *createTxt[] = {"touch", path, NULL};
    createProcess(keyTouch, createTxt);

    FILE *f = fopen(path, "w");
    if (f == NULL)
    {
      printf("Error when opening file %s \n", path);
      exit(1);
    }

    fprintf(f, "kategori: %s \n\n", whereFolder);

    insertionSort(arrTahun, arrFileName, 5);

    for (int i = 0; i < 5; i++)
    {
      if (arrTahun[i][0] != 0)
      {
        fprintf(f, "nama: %s \n", arrFileName[i]);
        fprintf(f, "rilis: tahun %d \n\n", arrTahun[i][0]);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}

void getListFolderDrakor()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("/home/naufalrama/shift2/drakor/");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0)
      {
        renameFileAndCreateTxtPerFolder(ep->d_name);
      }
    }
    (void)closedir(dp);
  }
  else
  {
    exit(0);
  }
}

int main()
{
  createParentFolder();
  extractZip();
  removeDir();
  createFolderDrakorByCategory();
  moveFileByCategory();
  getListFolderDrakor();
}
